require('./bootstrap');
Vue.component('datagrid', require('./components/Datagrid.vue'));
Vue.component('navigation', require('./components/Navigation.vue'));
import VeeValidate from 'vee-validate'; // used for contact form validation
Vue.use(VeeValidate);
import VueRouter from 'vue-router';
import About from './components/About.vue';
import Resume from './components/Resume.vue';
import Skills from './components/Skills.vue';
import Contact from './components/Contact.vue';
const DataGridExample = {
    'template': '<datagrid source="/api/customers" title="Data Grid Example" ' +
    'v-bind:headerColumns="[{dbField: \'id\', displayName: \'Id\'}, ' +
    '{dbField: \'name\', displayName: \'User Name\'}, ' +
    '{dbField: \'email\', displayName: \'User Email\'}, ' +
    '{dbField: \'phone\', displayName: \'Phone number\'}]"' +
    'v-bind:editableColumns="[\'name\', \'email\', \'phone\']"></datagrid>'
};
Vue.use(VueRouter);
const routes = [
    {path: '/about', component: About},
    {path: '/resume', component: Resume},
    {path: '/skills', component: Skills},
    {path: '/contact', component: Contact},
    {path: '/example', component: DataGridExample}
];
const router = new VueRouter({
    routes
});
const app = new Vue({
    router
}).$mount('#app');
router.mode = 'html5'
router.push('about');
