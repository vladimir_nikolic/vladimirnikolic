@extends('welcome')
@section('content')
    <div class="container Absolute-Center is-Responsive">
        <div id="middle_box">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <span><img src="{{ asset('images/vladimir.jpg') }}" class="img-responsive img-rounded pull-right" alt="Vladimir Nikolic" width="80" height="40"></span>

                <h3>\\Vladimir Nikolic</h3>
                <p>Fullstack Web developer with more then 12 years of experience</p>
                <ul class="nav nav-tabs nav-justified">
                    <li class="active">
                        <router-link to="about">Aboutd</router-link>
                    </li>
                    <li>
                        <router-link to="resume">Resume</router-link>
                    </li>
                    <li>
                        <router-link to="skills">Skills</router-link>
                    </li>
                    <li>
                        <router-link to="contact">Contact</router-link>
                    </li>
                </ul>
                <div class="well-lg">
                    sneka sekcija
                </div>
            </div>
            <div class="row-fluid footer">Built with Laravel 5.3 and Vue.js 2.0.8</div>
        </div>

    </div>
@endsection