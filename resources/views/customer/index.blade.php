@extends('welcome')
@section('content')
    <div class="container is-Responsive">
        <div id="middle_box">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <span>
                    <a href="/vcard">
                        <img src="{{ asset('images/card.png') }}" class="img-responsive img-rounded pull-right" alt="Vladimir Nikolic" title="Download vCard">
                    </a>
                </span>
                <h3><strong>&#60;&#63; php</strong> <i> /** Vladimir Nikolic **/</i></h3>
                <p>Web Developer with more then 12 years of experience!</p>
                <!-- <navigation></navigation> -->
                <navigation></navigation>

                <!-- route outlet -->
                <!-- component matched by the route will render here -->
                <div class="white">
                    @if($errors->all())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach
                    @endif

                        <router-view></router-view>

                </div>
            </div>
            <div class="row-fluid footer">Built with Laravel 5.3 and Vue.js 2.0 in ~20 hours</div>
        </div>

    </div>
@endsection