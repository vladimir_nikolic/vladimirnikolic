<?php

Route::get('/','CustomerController@index');
Route::get('api/customers', 'CustomerController@getData');
Route::get('/design', 'CustomerController@css');
Route::get('/vcard', 'CustomerController@vCard');
Route::post('/sendmail', 'CustomerController@sendmail');