<?php

use Illuminate\Database\Seeder;
use App\Customer;
use Faker\Factory as Faker;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        Customer::truncate();
        foreach(range(1,100000) as $index)
        {
            Customer::create([
                'name' => $faker->firstName,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber
            ]);
        }
    }
}