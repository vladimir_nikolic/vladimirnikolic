<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\DataCollector;
class Customer extends Model
{
    use DataCollector;
    protected $fillable = [
        'name',
        'email',
        'phone'
    ];
    public static $columns = [
        'id',
        'name',
        'email',
        'phone'
    ];
}
