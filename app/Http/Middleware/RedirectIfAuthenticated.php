<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (app()->isLocal() && isset($_COOKIE['selenium_request']))
        {
            config(['database.default' => 'mysql_testing']);
            if (isset($_COOKIE['selenium_auth']))
            {
                Auth::loginUsingId((int)$_COOKIE['selenium_auth']);
            }
        }
        if (Auth::guard($guard)->check())
        {
            return redirect('/home');
        }

        return $next($request);
    }
}
