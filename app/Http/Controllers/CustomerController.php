<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator as Validation;
use JeroenDesloovere\VCard\VCard;
use Illuminate\Contracts\Validation\Validator;
class CustomerController extends Controller
{
    /**
     * 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('customer.index');
    }

    /**
     * create a vCard
     *
     * @return void
     */
    public function vCard()
    {
        $vcard = new VCard();
        $lastname = 'PHP Coder';
        $firstname = 'Vladimir';
        $additional = 'Nikolic';
        $prefix = '';
        $suffix = '';
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);
        $vcard->addCompany('FBM');
        $vcard->addJobtitle('Full stack developer');
        $vcard->addEmail('nezaboravi@gmail.com');
        $vcard->addPhoneNumber(+66956789124, 'PREF;WORK; HOME');
        $vcard->addAddress(null, null, null, 'Chiang Mai', null, '50300', 'Thailand');
        $vcard->addURL('http://vladimirnikolic.info');
        $vcard->addPhoto('images/vladimir.jpg');
        return $vcard->download();
    }

    /**
     * fetch customers data from customers table
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {

        $model = Customer::search();
        $columns = Customer::$columns;
        return response()
            ->json([
                'model' => $model,
                'columns' => $columns
            ]);
    }
    /**
     * {@inheritdoc}
     */
    protected function formatErrors(Validator $validator)
    {

        return $validator->errors()->all();
    }
    public function sendmail(Request $request)
    {

        $validator = Validation::make($request->all(), [
            'message' => 'required|min:10|max:255',
            'name' => 'required|min:3',
            'from' => 'required|email'

        ]);
        $errors = $this->formatErrors($validator);
        $server_response['errors'] = [];
        foreach ($errors as $error)
        {
            $server_response['errors'][] = $error;
        }
        if ($validator->fails()) {
            return response()->json([
                'failed' => true
            ]);
        }
        Mail::send('emails.contact',
            array(
                'name' => $request->get('name'),
                'email' => $request->get('from'),
                'user_message' => $request->get('message')
            ), function($message)
            {
                $message->from('vladimir@vladimirnikolic.info');
                $message->to('nezaboravi@gmail.com', 'Admin')->subject('VladimirNikolic.info Contact');
            });

        return response()->json([
            'success' => true
        ]);


    }
}
