<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Validation\ValidatesRequests;
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 11/21/16
 * Time: 1:42 PM
 */
trait DataCollector
{
    use ValidatesRequests;
    protected static $operators = [
        'equal'                    => '=',
        'not_equal'                => '<>',
        'less_than'                => '<',
        'greater_than'             => '>',
        'less_than_or_equal_to'    => '<=',
        'greater_than_or_equal_to' => '>=',
        'in'                       => 'IN',
        'like'                     => 'LIKE'
    ];

    /**
     * Search
     *
     * @param string $query string
     *
     * @return mixed
     * @throws ValidationException
     */
    public static function scopeSearch($query)
    {
        $request = app()->make('request');
        return DB::table('customers')
                 ->select('id', 'name', 'email', 'phone')
                 ->orderBy($request->column, $request->direction)
                 ->where(function ($query) use ($request)
                 {

                     if ($request->has('search_input'))
                     {
                         if ($request->search_operator == 'in')
                         {
                             $query->whereIn($request->search_column, explode(',', $request->search_input));
                         }
                         else if ($request->search_operator == 'like')
                         {
                             $query->where($request->search_column, 'LIKE', '%' . $request->search_input . '%');
                         }
                         else
                         {
                             $query->where($request->search_column, self::$operators[$request->search_operator], $request->search_input);
                         }
                     }
                 })
                 ->paginate($request->per_page);
    }
}